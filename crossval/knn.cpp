#include<iostream>
#include<vector>
#include<cmath>
#include<map>
#include<algorithm>
#include<numeric>
#include<string>
#include<fstream>
#include<sstream>

//TGS 2610,TGS 2602,TGS 2620,TGS 2600,TGS 2611
/* Todos:
** -readTestData -------------------D O N E
** -allow dynamic dimension retrieval
** -numtoken = 4 -------------------D O N E
** -readTrainData automatically retrieve label names -------------------D O N E
** -read data, parsing ', ' automatically.
**
**
**
**
*/

const unsigned short dim = 5;

struct data{
	unsigned short label;
	double nval[dim];
};

double euclidean_distance(const data* a, const data* b){
	double distance = 0.0;
	for(int i= 0; i < dim; i++) distance += pow((a -> nval[i] - b -> nval[i] ), 2);
	return sqrt(distance);
}

int classifier(const data* test, const std::vector<data*> *v, const unsigned short k){
	if(k >= v -> size() or k < 1 ) return -1;
	std::vector<double>distances;
	for(int  i = 0; i < v -> size(); i++){
		distances.push_back(euclidean_distance(test, (*v)[i]));
		//std::cout << "Distance = " << distances.back() << "\n";//---------------display
	}

	//sort distances
	std::vector<int> indices(v -> size());
	std::iota(indices.begin(), indices.end(), 0);
	std::sort(indices.begin(), indices.end(),
		[&](int A, int B) -> bool{
			return distances[A] < distances [B];
		});

	//std::sort (distances.begin(), distances.end());

	//for(auto elem : distances) std::cout << elem << "\n"; //------display
	std::vector<int> nearestNeighbors;
	for(int i = 0; i < k; i++){
		/*std::cout << "Shortest neighbor for k = " << i <<" is " 
		<< (*v)[indices[i]] -> label << " with a distance of " << distances[indices[i]] << "\n";*/ //-----dispay
		nearestNeighbors.push_back((*v)[indices[i]] -> label);
	}

	//vote
	int max = 0;
	int most_common = -1;
	std::map<int,int> m;
	for (auto vi = nearestNeighbors.begin(); vi != nearestNeighbors.end(); vi++) {
		m[*vi]++;
		if (m[*vi] > max) {
		max = m[*vi];
		most_common = *vi;
  		}
	}

	return most_common;
}

void classify(std::vector<data*> *test, const std::vector<data*> *train, const unsigned short k){
	if(k >= train -> size() or k < 1 )std::cout << "The value 'k' cannot be bigger than the number of initial elements!\n";
	else{
		for(int i = 0; i < test -> size(); i++){
			(*test)[i] -> label = classifier((*test)[i], train, k);
		}
	}

}


void readTrainFile(std::string fileName, std::vector<data*> *v, std::vector<std::string> *labelNames){
	std::ifstream ifile(fileName);
	std::string line;

	if(ifile.is_open()){
		while(getline(ifile, line)){
			std::stringstream ss(line);
			int numtok = 0;
			data * input = new data;
			for(std::string token; ss >> token;){
				if(numtok >= (5 - dim) and numtok < 5){
					input -> nval[numtok - (5 - dim)] = std::stod(token);
				}
				else if(numtok == 5){
					std::vector<std::string>::iterator it = std::find(labelNames -> begin(), labelNames -> end(), token);
					if(it != labelNames -> end()){
						input -> label = std::distance(labelNames -> begin(), it);
					}
					else{
						labelNames -> push_back(token);
						input -> label = labelNames -> size() - 1;
					}

				}
				numtok++;
				if(ss.peek() == ',') ss.ignore();
			}
			v -> push_back(input);
		}
		ifile.close();
	}
	else std::cout << "Cannot open: " << fileName << "\n";

}

void readTestFile(std::string fileName, std::vector<data*> *v, std::vector<std::string> *labelNames, std::vector<unsigned short> *testLabel){
	std::ifstream ifile(fileName);
	std::string line;

	if(ifile.is_open()){
		while(getline(ifile, line)){
			std::stringstream ss(line);
			int numtok = 0;
			data * input = new data;
			for(std::string token; ss >> token;){
				if(numtok >= 0 and numtok < 5){
					input -> nval[numtok] = std::stod(token);
				}
				else if(numtok == 5){
					std::vector<std::string>::iterator it = std::find(labelNames -> begin(), labelNames -> end(), token);
					if(it != labelNames -> end()){
						testLabel -> push_back(int(std::distance(labelNames -> begin(), it)));
					}
					else{
						std::cout << "Unrecognizable token: "<< token << ", Pushing 0 instead \n";
						testLabel -> push_back(0);
					}
				}

				numtok++;
				if(ss.peek() == ',') ss.ignore();
			}
			v -> push_back(input);
		}
		ifile.close();
	}
	else std::cout << "Cannot open: " << fileName << "\n";
	
}

int main(int argc, char*argv[]){
	if(argc > 2){
		std::cerr << "Too many arguments, expecting one or none\n";
		return 1;
	}
	int tPos(0), tNeg(0), fPos(0), fNeg(0);

	std::vector<data*> train;
	std::vector<data*> test;
	std::vector<unsigned short> testLabel;
	std::vector<std::string> labelNames;
	for(int scenario = 0; scenario < 10; scenario++){

		std::cout << "\nScenario "<< scenario <<"\n";

		std::string trainFileName = "input" + std::to_string(scenario) + ".train";
		std::string testFileName = "input" + std::to_string(scenario) + ".test";
		readTrainFile(trainFileName, &train, &labelNames);
		readTestFile(testFileName, &test, &labelNames, &testLabel);

		const unsigned short k = ((argc == 1) ? int(sqrt(train.size())) : std::stoi(argv[1]));
		std::cout << "K = " << k << "\n";

		classify(&test, &train, k);

		std::cout << "Training datas: \n";
		while(!train.empty()){
			for(int i = 0; i < dim; i++) std::cout << train.back() -> nval[i] << "\t";
			std::cout << labelNames[train.back() -> label] << "\n";
			delete train.back();
			train.pop_back();
		}

		std::cout << "\nTesting datas for k = " << k <<":\n";
		while(!test.empty()){
			for(int i = 0; i < dim; i++) std::cout << test.back() -> nval[i] << "\t";
			std::cout << labelNames[test.back() -> label] << "\t";
			if(test.back() -> label == testLabel.back()){
				if(testLabel.back()) tPos += 1;
				else tNeg += 1;
				std::cout << "CORRECT\n";
			}
			else{
				if(testLabel.back()) fPos += 1;
				else fNeg += 1;
				std::cout << "INCORRECT\n";
			}
			delete test.back();
			test.pop_back();
			testLabel.pop_back();
		}
	}

	std::cout << "\nCross-Validation Result:\n"
		<< "True Positive: " << tPos << "\n"
		<< "True Negative: " << tNeg << "\n"
		<< "False Positive: " << fPos << "\n"
		<< "False Negative: " << fNeg << "\n";

	float accuracy, precision, recall, f1;
	accuracy = float(tPos + tNeg)/float(tPos + tNeg + fPos + fNeg);
	precision = float(tPos)/float(tPos + fPos);
	recall = float(tPos)/float(tPos + fNeg);
	f1 = 2.f * float(precision * recall)/float(precision + recall);
	
	std::cout << "\nPerformance: \n"
		<< "accuracy: " << accuracy << "\n"
		<< "precision: " << precision << "\n"
		<< "recall: " << recall << "\n"
		<< "f1 score: " << f1 << "\n";

}
