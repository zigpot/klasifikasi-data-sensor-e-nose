# Klasifikasi Tahu Berformalin dan Murni dari Data Sensor



## Instalasi

Program dapat di-*compile* dengan kompilator C++ pilihan.

```bash
g++ read_data.cpp -o read_data
g++ knn.cpp -o knn
g++ nb.cpp -o nb
```

## Penggunaan
Untuk penggunaan pertama jalankan: ```./read_data```. Program ini membaca file "data_sensor.csv" dan menghasilkan output "input.test" dan "input.train", yang berisi 90% data latih dan 10% data uji, untuk masing-masing kategori (tahu berformalin dan murni). Pembagian data dilakukan secara random.

Ada enam metode ekstraksi ciri yang dapat digunakan **flag number**, yakni:
1. *integral*	    **0**
2. *difference*	**1** (default)
3. *mean*          **2**
4. *variance*      **3**
5. *relative*      **4** 
6. *fractional*    **5**

Bila ingin menggunakan ekstraksi ciri selain default, maka program dijalankan diikuti argumen __flag number__. Contoh untuk ekstraksi ciri *variance*:
```./read_data 3```

Untuk klasifikasi ada dua opsi:

### k-Nearest Neighbors
Untuk menjalankan, gunakan perintah ```./knn``` diikuti nilai *k*. Contoh:

```./knn 7```

Bila tak ada argumen, maka secara default:

&ensp; k ≈ sqrt(N), N = jumlah data latih
### Gaussian Naïve Bayes
Cukup dengan perintah: ```./nb```

## Penggunaan (Kasus *Cross Validation*)
Pada subdirectory "crossval" terdapat tiga program serupa yang dapat di-*compile*.
Cara penggunaan sama seperti sebelumnya, namun pada *cross validation*,  ```./read_data``` menghasilkan fitur data latih "input0.train", "input1.train", "input2.train", hingga "input9.train". Demikian juga data uji "input0.test" hingga "input9.test".

## dan lain-lain
untuk kontak: fariz.adnan.w[at]mail.ugm.ac.id

## Read More
*Identifikasi Tahu Berformalin dengan Electronic Nose Menggunakan Jaringan Syaraf Tiruan Backpropagation* https://doi.org/10.22146/ijeis.15330