#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<queue>
#include<vector>
#include<algorithm>
#include<random>
#include<ctime>

#define NumOfSensors 5 //jumlah atribut; yakni masing-masing sensor

typedef float(*func)(std::vector<double>::iterator begin,std::vector<double>::iterator end);

const std::string methodNames[6] = {
		"integral",
		"difference",
		"mean",
		"variance",
		"relative",
		"fractional"};

float extractFeature0(std::vector<double>::iterator begin, std::vector<double>::iterator end){
	return std::accumulate(begin, end, 0);
}

float extractFeature1(std::vector<double>::iterator begin, std::vector<double>::iterator end){
	std::vector<double>::iterator min = std::min_element(begin, end);
	std::vector<double>::iterator max = std::max_element(begin, end);

	return *max - *min;
}

float extractFeature2(std::vector<double>::iterator begin, std::vector<double>::iterator end){
	std::vector<double>::iterator min = std::min_element(begin, end);
	std::vector<double>::iterator max = std::max_element(begin, end);

	return extractFeature0(begin, end) / std::distance(begin, end);
}

float extractFeature3(std::vector<double>::iterator begin, std::vector<double>::iterator end){
	float mean_ = extractFeature2(begin, end);
	float sum(0);
	for(auto i = 0; begin + i != end; i++){
		int elem = *(begin + i);
		sum += ((elem - mean_) * (elem - mean_));
	}
	return sum / (std::distance(begin, end) - 1);
}

float extractFeature4(std::vector<double>::iterator begin, std::vector<double>::iterator end){
	std::vector<double>::iterator min = std::min_element(begin, end);
	std::vector<double>::iterator max = std::max_element(begin, end);

	return *max / *min;
}

float extractFeature5(std::vector<double>::iterator begin, std::vector<double>::iterator end){
	std::vector<double>::iterator min = std::min_element(begin, end);
	std::vector<double>::iterator max = std::max_element(begin, end);

	return *max / *min - 1;
}

void printExample(char * progName){
	std::cout << "Usage example: " << progName << " n\n";
	std::cout << "Where n is the feature extraction method of choice."
		<<"Here's a list of method with the corresponding flag number:\n\n"
		<<"\tintegral 0\n"
		<<"\tdifference 1\n"
		<<"\tmean 2\n"
		<<"\tvariance 3\n"
		<<"\trelative 4\n"
		<<"\tfractional 5\n"
		<< "\nFor example, to extract feature using fractional method:\n"
		<< progName << " 5\n";
}


int main(int argc, char* argv[]){
	int extMethod = 1;
	if(argc > 2){
		std::cerr << "Too many arguments, expecting one or none\n";
		printExample(argv[0]);
		return 1;
	}

	if(argc == 2){
		extMethod = argv[1][0]-48;
		if(extMethod > 5 or extMethod < 0){
			std::cerr << "Invalid argument\n";
			printExample(argv[0]);
			return 1;
		}
	}

	std::string filename("data_sensor.csv");
	std::queue<double> data[NumOfSensors];
	//pada csv (berformalin = 1, tidak berformalin = -1) 
	//menjadi (berformalin = true, tidak berformalin = false)
	std::queue<bool> class_; 
	std::ifstream file(filename);
	std::string line;
	func extractFeature[] = {
		extractFeature0,
		extractFeature1,
		extractFeature2,
		extractFeature3,
		extractFeature4,
		extractFeature5
	};

	if(file.is_open()){
		int num = 0;
		int lines = 0;
		while(getline(file, line)){
			lines++;
			if(lines > 1){
				std::stringstream ss(line);
				for(double i; ss >> i;){
					if(num < NumOfSensors){ 
						data[num].push(i);
						//std::cout << i <<"\t"; //--------output
						}
					else{
						int truth = ((i + 1)/2);
						//std::cout << (truth ? "B" : "Tidak b") << "erformalin"; //------output
						class_.push(truth);
					}
					if (ss.peek() == ',')
						ss.ignore();
					num++;
				}
				//std::cout << "\n";// ---------------output
				num = 0;
			}

		}
		file.close();
	}
	else{std::cout << "Failed to open file!\n";}
	/* ----------------output
	if(data[1].size()){
		std::cout << lines << "\n";
		std::cout << data[1].size() << "\n";
	}*/

	std::vector<std::vector<double>> FORMALIN(NumOfSensors);
	std::vector<std::vector<double>> MURNI(NumOfSensors);

	std::ofstream trainFile("input.train");
	std::ofstream testFile("input.test");
	std::ofstream *File;

	while(!class_.empty()){
		if(class_.front()){
			//std::cout << "Berformalin\n"; //------output
			for(int i = 0; i < NumOfSensors; i++){
			FORMALIN[i].push_back(data[i].front());
			data[i].pop();
			}

		}
		else{
			//std::cout <<"Tidak berformalin\n"; //------output
			for(int i = 0; i < NumOfSensors; i++){
			MURNI[i].push_back(data[i].front());
			data[i].pop();
			}
		}

		class_.pop();
	}

	//let's randomize

	srand(std::time(nullptr));
	int iTest_0(rand() % 10), iTest_1(rand() % 10);

	std::cout << "Daftar Fitur. "; //-----------------output
	std::cout << "Metode ekstraksi: "<< methodNames[extMethod] <<"\n"; //-----------------output
	for(int i = 0; i < 10; i++){
		if(i == iTest_0) File = &testFile;
		else File = &trainFile;
		//berformalin
		for(int j = 0; j < NumOfSensors; j++){
			double feature = extractFeature[extMethod](FORMALIN[j].begin() + i * 300, FORMALIN[j].begin() + i * 300 + 300);
			std::cout << feature << "\t";
			*File << feature <<  "\t";
		}
		std::cout << "FORMALIN\n";
		*File << "FORMALIN\n";

	}

	for(int i = 0; i < 10; i++){
		if(i == iTest_1) File = &testFile;
		else File = &trainFile;
		//tidak berformalin
		for(int j = 0; j < NumOfSensors; j++){
			double feature = extractFeature[extMethod](MURNI[j].begin() + i * 300, MURNI[j].begin() + i * 300 + 300);
			std::cout << feature << "\t";
			*File << feature <<  "\t";
		}
		std::cout << "MURNI\n";
		*File << "MURNI\n";

	}

}
