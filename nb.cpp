#include<cmath>
#include<numeric>
#include<algorithm>
#include<vector>
#include<string>
#include<fstream>
#include<sstream>
#include<iostream>

/* Todos:
** -allow dynamic dimension retrieval
** -allow dynamic classes retrieval
*/

const short unsigned dim = 5;
const short unsigned classes = 2;

struct stat{
	float mean;
	float stdev;
};

struct data{
	int label;
	float nval[dim];
};

float prob(float x, float y){
	return log(x) - log(y);
}

float normal_pdf(const stat S, float x)
{
    static const float inv_sqrt_2pi = 0.3989422804014327;
    float a = (x - S.mean) / S.stdev;

    return log(inv_sqrt_2pi / S.stdev) + (-0.5f * a * a);
}

float mean(const std::vector<float>* datas){
	float N = datas -> size();
	float sum(0);
	for(auto elem : *datas){
		sum += elem;
	}
	return sum / (N);
}

float stdev(const std::vector<float>* datas){
	float mean_ = mean(datas);
	float N = datas -> size();
	float sum(0);
	for(auto elem : *datas){
		sum += ((elem - mean_) * (elem - mean_));
	}
	return sqrt(sum / (N - 1));
}

void trainData(std::vector<data*> *train, std::vector<stat*> *S, std::vector<int> *occurence){
	//we're declaring a 2d vector of vectors (basically a 3d).
	//The 2d vector itself is implented as a 1d vector indexed as vector[dim * i + j], 0 ≤ i < classes and 0 ≤ j < dim.
	std::vector<std::vector<float>> v(dim * classes);

	for(int itrain = 0; itrain < train -> size(); itrain++){
		int i = (*train)[itrain] -> label;
			for(int j = 0; j < dim; j++){
				v[dim * i + j].push_back((*train)[itrain] -> nval[j]);
			}
			(*occurence)[i] ++;
	}
	
	for(int i = 0; i < classes; i++){
		for(int j = 0; j < dim; j++){
			stat * stat_ = new stat;
			std::vector<float> vecTemp = v[dim * i + j];
			stat_ -> mean = mean(&vecTemp);
			stat_ -> stdev = stdev(&vecTemp);
			S -> push_back(stat_);
		}
	}

}

void testData(std::vector<data*> *test, std::vector<stat*> *S, const std::vector<int> *occurence){
	std::vector<float> p(classes, 0);
	const int all_occurence = std::accumulate(occurence -> begin(), occurence -> end(), 0);
	for(int k = 0; k < test -> size(); k++){
		for(int i = 0; i < classes; i++){
			p[i] += prob((*occurence)[i], all_occurence);
			for(int j = 0; j < dim; j++){
				float xk = normal_pdf(*(*S)[dim * i + j], (*test)[k] -> nval[j]);
				p[i] += xk;
			}
		}
		
		(*test)[k] -> label = std::distance( p.begin(), std::max_element(p.begin(), p.end()));
		std::fill(p.begin(), p.end(), 0);
	}
}

void readTrainFile(std::string fileName, std::vector<data*> *v, std::vector<std::string> *labelNames){
	std::ifstream ifile(fileName);
	std::string line;

	if(ifile.is_open()){
		while(getline(ifile, line)){
			std::stringstream ss(line);
			int numtok = 0;
			data * input = new data;
			for(std::string token; ss >> token;){
				if(numtok < dim){
					input -> nval[numtok] = std::stod(token);
				}
				else if(numtok == dim){
					std::vector<std::string>::iterator it = std::find(labelNames -> begin(), labelNames -> end(), token);
					if(it != labelNames -> end()){
						input -> label = std::distance(labelNames -> begin(), it);
					}
					else{
						labelNames -> push_back(token);
						input -> label = labelNames -> size() - 1;
					}

				}
				numtok++;
				if(ss.peek() == ',') ss.ignore();
			}
			v -> push_back(input);
		}
		ifile.close();
	}
	else std::cout << "Cannot open: " << fileName << "\n";

}

void readTestFile(std::string fileName, std::vector<data*> *v, std::vector<std::string> *labelNames, std::vector<short unsigned> *testLabel){
	std::ifstream ifile(fileName);
	std::string line;

	if(ifile.is_open()){
		while(getline(ifile, line)){
			std::stringstream ss(line);
			int numtok = 0;
			data * input = new data;
			for(std::string token; ss >> token;){
				if(numtok < 5){
					input -> nval[numtok] = std::stod(token);
				}
				else if(numtok == 5){
					std::vector<std::string>::iterator it = std::find(labelNames -> begin(), labelNames -> end(), token);
					if(it != labelNames -> end()){
						testLabel -> push_back(int(std::distance(labelNames -> begin(), it)));
					}
					else{
						std::cout << "Unrecognizable token: "<< token << ", Pushing 0 instead \n";
						testLabel -> push_back(0);
					}
				}
				numtok++;
				if(ss.peek() == ',') ss.ignore();
			}
			v -> push_back(input);
		}
		ifile.close();
	}
	else std::cout << "Cannot open: " << fileName << "\n";
	
}


int main(){
	std::vector<data*> train;
	std::vector<data*> test;
	std::vector<std::string> labelNames;
	std::vector<unsigned short> testLabel;
	std::vector<int> label;
	std::vector<int> occurence(classes, 0);
	std::vector<stat*> S;
	readTrainFile("input.train", &train, &labelNames);

	readTestFile("input.test", &test, &labelNames, &testLabel);

	trainData(&train, &S, &occurence);
	testData(&test, &S, &occurence);

	std::cout << "Data uji\n";
	while(!train.empty()){
		for (int x = 0; x < dim; x++) std::cout << train.back() -> nval[x] << "\t";
		std::cout << labelNames[train.back() -> label] <<"\n";
		delete train.back();
		train.pop_back();
	}

	std::cout << "\nPrediksi data latih:\n";
	while(!test.empty()){
		for (int x = 0; x < dim; x++) std::cout << test.back() -> nval[x] << "\t";
		std::cout << labelNames[test.back() -> label] <<"\t";
		std::cout << ((testLabel.back() == test.back() -> label) ? "CORRECT\n" :"INCORRECT\n");
		delete test.back();
		test.pop_back();
		testLabel.pop_back();
	}

	while(!S.empty()){
		delete S.back();
		S.pop_back();
	}

}
